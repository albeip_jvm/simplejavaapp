package com.albeip

class Hello {

    companion object {

        @JvmStatic
        fun sayHello() = "Hello World from Kotlin!!!"
    }

}